var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
var timer;
if (typeof check_seconds == 'undefined') {
	var check_seconds = 3000;
}


// *** injects iframe tag and player into the DOM where your div is located
var YTListeners = []; // support multiple players on the same page
function onYouTubePlayerAPIReady() {
	//for (var e = document.getElementsByClassName('wf-yt-video'), x = e.length; x--;) {
	$(".wf-yt-video").each(function (i, obj) {
		YTListeners.push(new YT.Player(obj, {
			height: $(obj).data('height'),
			width: $(obj).data('width'),
			//videoId: $('.wf-yt-video').eq(x).data('youtube-id'),
			videoId: $(obj).data('youtube-id'),
			playerVars: {
				'rel': 0,
				'showinfo': 0,
				'color': 'white'
			},
			events: {
				//these event elements tell the Youtube API which of your functions to call when its player executes these events
				'onReady': runWhenReady,
				'onStateChange': runWhenStateChange
			}
		}));
	});
}

// *** YouTube API calls this function when its player code is ready - as defined  in the JSON object in its injection code.
function runWhenReady(event) {
	//uncomment to autoplay on page load
	//event.target.playVideo();
	//uncomment next line to see if your videos are being detected
	//console.log('Video located. ' + event.target.a.attributes['data-name'].value + ' has a duration of: ' + event.target.getDuration());
}

// *** YouTube API calls this function when its player state has changed - as defined  in the JSON object in its injection code.
function runWhenStateChange(event) {
	// gets video info such as video_data.author, video_data.title, video_data.video_id
	//var video_data = event.target["getVideoData"]();
	var video_name = event.target.a.attributes['data-name'].value;
	var video_id = event.target.a.attributes["data-youtube-id"].value;

	switch (event.target["getPlayerState"]()) {
		case YT.PlayerState.PLAYING:
			//per ADA, only one video is allowed to play at one time
			//so we pause all other videos
			pauseOtherPlayingVids(video_id);

			//video is playing, so let's start tracking.
			trackVideoProgress(event["target"]);
			break;
		case YT.PlayerState.PAUSED:
			break;
		case YT.PlayerState.ENDED:
			clearTimeout(timer);
			for (let i = 0; i < timestamps[video_name].length; i++) {
				if (timestamps[video_name][i].trigger == 1) {
					sendTrackingdata(timestamps[video_name][i]);
				}
			}
			break;
	}
}

// report the % played if it matches the JSON data object
function trackVideoProgress(event) {


	let isPlaying = event["getPlayerState"]() == YT.PlayerState.PLAYING;

	if (isPlaying) {

		event["curPercent"] = parseFloat((event["getCurrentTime"]() / event["getDuration"]()).toFixed(2));


		var video_name = event.a.attributes['data-name'].value;
		for (let i = 0; i < timestamps[video_name].length; i++) {
			if (
				(event["curPercent"] > timestamps[video_name][i].trigger && timestamps[video_name][i].trigger > event["lastTrigger"])
				||
				typeof event["lastTrigger"] == 'undefined'
			) {

				event["lastTrigger"] = timestamps[video_name][i].trigger;
				sendTrackingdata(timestamps[video_name][i]);
			}

		}
		if (event["curPercent"] < 1 && event["getPlayerState"]() == YT.PlayerState.PLAYING) {
			timer = setTimeout(trackVideoProgress, check_seconds, event);
		}
	}
}

function sendTrackingdata(obj) {
	//console.log('obj.cat is: ' + obj.cat + '.');
	//console.log('obj.action is: ' + obj.action + '.');
	//console.log('obj.label is: ' + obj.label + '.');

	if (typeof obj != "undefined") {
		/////////////////////////////////////////////////////////////
		// Google Video tracking
		/////////////////////////////////////////////////////////////
		if (obj.google) {
			var feed = {
				event: obj.google.event,
				lineOfBusiness: obj.google.cat,
				userActionTaken: obj.google.action,
				subproduct: obj.google.label,
				buttonLocation: obj.google.location,
			};
			window.dataLayer.push(feed);
		}

		/////////////////////////////////////////////////////////////
		// DCM Floodlight Video tracking
		/////////////////////////////////////////////////////////////
		if (obj.floodlight) {
			var rand = Math.floor((Math.random() + "") * 10000000000000000);
			var src = "https://2549153.fls.doubleclick.net/activityi;src=2549153;type=" + obj.floodlight.type + ";cat=" + obj.floodlight.cat + ";dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=" + rand + "?";
			var iframe = document.createElement('iframe');
			iframe.style.display = "none";
			iframe.src = src;
			document.body.appendChild(iframe);
		}

		/////////////////////////////////////////////////////////////
		// add any other obj.xxx tracking conditionals here as necessary.
		// just make sure you have those nodes in your JSON tracking object (see tracking.js)
		/////////////////////////////////////////////////////////////

	}
}

var curVideoPlaying;
function pauseOtherPlayingVids(id) {
	curVideoPlaying = id;
	for (let i = 0; i < YTListeners.length; i++) {
		if (YTListeners[i].getVideoData().video_id != curVideoPlaying) {
			YTListeners[i]['pauseVideo']();
		}
	}
}
